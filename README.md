# Elementary OS auto setup for daily usage

# Changelog
Replaced:
* [x] Added Monitor instead of Stacer
* [x] Apostrophe instead of the old one
* [x] Eddy instead of gdebi
* [x] Added Desktop Folder
* [x] Fixed dock configurator thanks to u/brigcaster
* [x] Everything should happen now in a temporary folder that will be deleted at the end of the script thanks to u/ojdorson


Everything should work just fine now!

# Intro
A simple script to get eOS to a daily distro state.

Inspired by the user u/brigcaster I decided to do a simple script that will get eOS to fullfill the needs of a basic user.
Not everyone is a programmer or an advanced user. 
People like me need eOS because is good looking, fast and privacy is enough. But some things arent there from the start and I try to fix this.

I will explain every part of the script below so you will know what you will get after using that script and how to use it without using advanced terms.

# Installation
Now that you know what you will get I need to explain the installations.
1. Set the script so it can be executed by using this command: sudo chmod +x /path/to/file/eOS_setup.sh (most of time /home/username/Downloads/eOS_setup.sh)
2. Run it by using the command: sudo sh /path/to/file/eOS_setup.sh

# Essentials
This part of the code will get your fresh installation of eOS to the lastest version of it so you will have the latest public fixes and performance improvements

Also it will install some basic utilities:

* software-properties-common - so you can add PPAs
* gnome-disk-utility - an useful application to mount/unmount/ drives
* wine-stable - so you can run some of the windows .exe application on Linux
* apt-transport-https curl git - these 3 thing are necessary for getting things from inernet by using terminal
* ubuntu-restricted-extras libavcodec-extra - some codecs that are needed for media files
* rar unrar p7zip-rar p7zip sharutils uudeview mpack arj cabextract lzip lunzip - these are useful when you need to open some archives, and as a basic user you would
* vlc - popular media player for mp4 mp3 and many kind of media files
* gparted - partition manager so you can resize, edit partitions
* preload - useful app that learn your use of applications so they will load faster
* nautilus - file manager, pretty easy to use
* snapd - add snap packages functionality


# Brave
Brave Browser is based on Chrome but with privacy features. Also you can earn money by viewing ads. 

# Monitor
Simple app to see the resources that are used and the services that are running.

# Eddy
An installer for .deb packages so you dont overuse terminal :)

# Apostrophe
Easy to use app for writing purposes

# Desktop Folder
To bring back the menu on right-click on desktop.

# tlp
useful for laptop as it will manage the battery life

# eOS tweaks
add a menu entry where you can change some settings like dark mode icons themes etc.

# System Tray Icons
System Tray shows apps that are running in background in wingpanel

# ePapirus Icon theme
Gorgeous icons with support for eOS so the wingpannel doesnt go wild

# Dark theme
Enable the dark theme so your eyes will take a break from that white

# Flatpak Apps
Adds 
Zoom - video conferences apps
Discord - voIP client for gamers
Glimpse - image editor like PS
Apostrophe - distraction free text editor

# Wallpaper
sets the wallpaper to a darker one so it will look well with the icons and the dark theme

# Dock configurator
Clear the dock and add the items that you would use often

I tried to make this efortless so after getting it running you dont need to do anything just to wait.