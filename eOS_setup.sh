#!/usr/bin/env bash

# Flatpak Apps
flatpak --user remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak --user install flathub \
us.zoom.Zoom \
com.discordapp.Discord \
org.gnome.gitlab.somas.Apostrophe \
org.glimpse_editor.Glimpse -y

# Temporary Folder
mkdir /tmp/eOSetup
cd /tmp/eOSetup

# Essentials
sudo apt update && sudo apt upgrade -y
sudo apt install -y software-properties-common gnome-disk-utility wine-stable apt-transport-https curl git ubuntu-restricted-extras libavcodec-extra rar unrar p7zip-rar p7zip sharutils uudeview mpack arj cabextract lzip lunzip vlc gparted preload nautilus snapd

# Brave
sudo apt install -y apt-transport-https curl
curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc \
     | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -
echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" \
     | sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt update
sudo apt install -y brave-browser

# Monitor
sudo apt install -y com.github.stsdc.monitor

# Desktop Folder - bring back the folders
sudo apt install -y com.github.spheras.desktopfolder

# Eddy - easy to use .deb installer
sudo apt install -y com.github.donadigo.eddy

# tlp
sudo add-apt-repository -y ppa:linrunner/tlp
sudo apt install -y tlp tlp-rdw
sudo tlp start

# eOS tweaks
sudo add-apt-repository -y ppa:philip.scott/elementary-tweaks
sudo apt install -y elementary-tweaks

# System Tray Icons are back
sudo apt install -y gobject-introspection libglib2.0-dev libgranite-dev libindicator3-dev libwingpanel-2.0-dev valac meson
git clone https://github.com/donadigo/wingpanel-indicator-namarupa
cd wingpanel-indicator-namarupa
sudo meson build --prefix=/usr && cd build && ninja
sudo ninja install && apt install -f
wget https://github.com/mdh34/elementary-indicators/releases/download/0.1/indicator-application-patched.deb
sudo dpkg -i indicator-application-patched.deb
sudo apt-mark hold indicator-application

# ePapirus Icon theme
wget -qO- https://git.io/papirus-icon-theme-install | sh
gsettings set org.gnome.desktop.interface icon-theme 'ePapirus'

# Dark theme
gsettings set io.elementary.terminal.settings prefer-dark-style true

# Wallpaper
dconf write /org/gnome/desktop/background/picture-uri "'file:///usr/share/backgrounds/Julia Craice.jpg'"
# you can replace it with whichever picture you want, but please keep the format "'<picture-uri>'"

# Plank configurator
sudo rm ~/.config/plank/dock1/launchers/*.dockitem

DockItems=("/usr/share/applications/brave-browser.desktop" \
	    "/usr/share/applications/nautilus.desktop" \
      "/usr/share/applications/io.elementary.terminal.desktop" \
	    "/usr/share/applications/io.elementary.appcenter.desktop" \
	    "/usr/share/applications/io.elementary.switchboard.desktop" \
	    "/usr/share/applications/com.github.stsdc.monitor.desktop" \
      "$HOME/.local/share/flatpak/exports/share/applications/org.gnome.gitlab.somas.Apostrophe.desktop" \
      "$HOME/.local/share/flatpak/exports/share/applications/org.glimpse_editor.Glimpse.desktop" \
      "/usr/share/applications/io.elementary.music.desktop")

for item in "${DockItems[@]}"; do
echo -e "[PlankDockItemPreferences]\nLauncher=file://$item" > "$HOME"/.config/plank/dock1/launchers/"$(basename -- "$item")".dockitem
echo "$item"
echo "$(basename -- $item)"
done

# End of Temporary Folder
cd /tmp
sudo rm -rf eOSetup

# Cleaning
sudo apt autoremove -y && sudo apt -y clean
sudo apt autoclean -y

# A reboot is needed
sudo reboot